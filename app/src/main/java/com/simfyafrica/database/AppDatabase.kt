package com.simfyafrica.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.simfyafrica.models.*


@Database(entities = [ImageItem::class, PokeResponse::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun imageDao(): ImageDao
    abstract fun pokeDao(): PokeResponseDao
}