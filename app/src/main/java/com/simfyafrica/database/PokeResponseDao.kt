package com.simfyafrica.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import com.simfyafrica.models.PokeResponse


@Dao
interface PokeResponseDao {


    @Insert(onConflict = REPLACE)
    fun insert(pokeData: PokeResponse)

    @Query("DELETE FROM poke_data WHERE id = :id")
    fun deleteElement(id: Long)


    @Query("DELETE FROM poke_data")
    fun deleteAll()

    @Query("Select storageLocation FROM poke_data where id = :id")
    fun getLocation(id: Long): Int

    @Query("Select security FROM poke_data where id = :id")
    fun getSecurityType(id: Long): Int

    @Query("SELECT * FROM poke_data WHERE id = :id")
    fun getResponse(id: Long): List<PokeResponse>

    @Query("SELECT * FROM poke_data")
    fun getAll(): List<PokeResponse>
}