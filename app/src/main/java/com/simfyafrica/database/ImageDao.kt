package com.simfyafrica.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import com.simfyafrica.models.ImageItem

@Dao
interface ImageDao {

    @Query("SELECT * FROM images")
    fun getImages(): MutableList<ImageItem>


    @Insert(onConflict = REPLACE)
    fun insert(images: List<ImageItem>)


    @Query("DELETE FROM images")
    fun deleteAll()


}