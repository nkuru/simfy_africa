package com.simfyafrica.utils

import android.os.Environment
import com.simfyafrica.app.SimfyAfricaApp
import se.simbio.encryption.Encryption
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.security.NoSuchAlgorithmException


class FileUtils {

    companion object {

        fun createFile(number: String, data: String?): String? {
            return if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {
                var file = getFileFromExternalStorage(number)
                val os = FileOutputStream(file)
                data?.let { os.write(data.toByteArray()) }
                os.close()
                null
            } else {
                "External storage does not exist or is not mounted"
            }
        }

        fun deleteFile(fileName: String?): String? {
            var file = getFileFromExternalStorage(fileName!!)
            if (file!!.exists()) {
                file.delete()
                return null
            }
            file = getFileFromInternalStorage(fileName)
            if (file!!.exists()) {
                file.delete()
                return null
            }

            return "Some error happened"

        }

        fun moveFile(fileName: String?): String? {
            val file1 = getFileFromExternalStorage(fileName!!)
            val file2 = getFileFromInternalStorage(fileName)

            if (!file1!!.exists() && !file2!!.exists()) return "Some error happened"
            val src: File
            val dest: File
            if (file1.exists()) {
                src = file1
                dest = file2!!
            } else {
                src = file2!!
                dest = file1
            }
            val inChannel = FileInputStream(src).channel
            val outChannel = FileOutputStream(dest).channel
            try {
                inChannel!!.transferTo(0, inChannel.size(), outChannel)
                src.delete()
            } catch (e: Exception) {
                return "Some error happened"
            } finally {
                inChannel?.close()
                outChannel?.close()
            }
            return null
        }

        fun encryptFile(fileName: String?, encryptionKey: String): String? {
            var file = getFileFromExternalStorage(fileName!!)
            if (!file!!.exists()) {
                file = getFileFromInternalStorage(fileName)
            }
            if (file!!.exists()) {
                var encryption: Encryption? = null
                try {
                    encryption = Encryption.Builder.getDefaultBuilder(encryptionKey, "a12bc34de56fg", ByteArray(16))
                            .setIterationCount(1) // use 1 instead the default of 65536
                            .build()
                } catch (e: NoSuchAlgorithmException) {
                    return "Some error happened"
                }
                File(file.absolutePath).writeText(encryption.encrypt(file.readText()))
            }

            return null

        }

        fun decryptFile(fileName: String?, encryptionKey: String): String? {
            var file = getFileFromExternalStorage(fileName!!)
            if (!file!!.exists()) {
                file = getFileFromInternalStorage(fileName)
            }
            if (file!!.exists()) {
                var encryption: Encryption? = null
                try {
                    encryption = Encryption.Builder.getDefaultBuilder(encryptionKey, "a12bc34de56fg", ByteArray(16))
                            .setIterationCount(1) // use 1 instead the default of 65536
                            .build()
                } catch (e: NoSuchAlgorithmException) {
                    return "Some error happened"
                }
                File(file.absolutePath).writeText(encryption.decrypt(file.readText()))
            }

            return null

        }

        fun resetAllFiles(): String? {
            val internalPath = Environment.getExternalStorageDirectory()
            var folder1 = File("${internalPath.absolutePath}${File.separator}Simfy Africa")
            if (folder1.exists() && folder1.isDirectory) {
                for (file in folder1.listFiles()) {
                    file.delete()
                }
            }
            var folder2 = File("${SimfyAfricaApp.application.filesDir.absolutePath}${File.separator}Simfy Africa")
            if (folder2.exists() && folder2.isDirectory) {
                for (file in folder2.listFiles()) {
                    file.delete()
                }
            }
            /*var storagePaths = ContextCompat.getExternalFilesDirs(SimfyAfricaApp.application, null)
            if (storagePaths.size >= 2) {
                File("${storagePaths[1].absolutePath}${File.separator}Simfy Africa${File.separator}").delete()
            }*/
            return null
        }

        fun fileExists(fileName: String?): Boolean {
            return fileName?.let { getFileFromExternalStorage(it)!!.exists() }!!
                    || getFileFromInternalStorage(fileName)!!.exists()
        }

        private fun getFileFromExternalStorage(number: String): File? {
            if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {
                val sdcard = Environment.getExternalStorageDirectory()
                val dir = File("${sdcard.absolutePath}${File.separator}Simfy Africa${File.separator}")
                dir.mkdirs()
                return File(dir, "$number.txt")
            }
            return null
        }


        private fun getFileFromInternalStorage(number: String): File? {
            File("${SimfyAfricaApp.application.filesDir.absolutePath}${File.separator}Simfy Africa").mkdirs()

            return File("${SimfyAfricaApp.application.filesDir.absolutePath}${File.separator}Simfy Africa", "$number.txt")
        }


        //FIXME -- The question speaks of external vs internal storage. This is not clear as I do not know if by external you mean external storage or external SD card. If you mean external SD card the following method (commented out) would be used

        /*private fun getFileFromSdCard(number: String): File? {
            val storagePaths = ContextCompat.getExternalFilesDirs(SimfyAfricaApp.application, null)
            if (storagePaths.size < 2) {
                return null
            }
            val dir = File("${storagePaths[1].path}${File.separator}Simfy Africa${File.separator}")
            dir.mkdirs()
            return File(dir, "$number.txt")
        }*/


    }
}