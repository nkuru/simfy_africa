package com.simfyafrica.utils


import android.app.ProgressDialog
import android.content.Context
import android.graphics.PorterDuff
import android.support.v4.content.ContextCompat
import android.widget.ProgressBar
import com.bumptech.glide.Glide.init
import com.simfyafrica.R

class ProgressDialogHelper(private val context: Context) {

    private val title: String

    private val message: String

    private var dialog: ProgressDialog? = null

    init {

        val resources = context.resources

        title = resources.getString(R.string.processing)
        message = resources.getString(R.string.please_wait)
    }

    private fun showProgressDialog(title: String, message: String = "") {
        dismissDialog()

        dialog = ProgressDialog.show(context, title, message, true, true)

        val progressBar = dialog!!.findViewById<ProgressBar>(android.R.id.progress)
        val drawable = progressBar.indeterminateDrawable
        drawable?.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary),
                PorterDuff.Mode.SRC_IN)
    }

    fun showProgressDialog() = showProgressDialog(title, message)

    fun dismissDialog() {
        if (dialog != null) {
            dialog!!.dismiss()
            dialog = null
        }
    }
}