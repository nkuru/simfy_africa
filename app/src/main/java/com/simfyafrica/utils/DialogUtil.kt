package com.simfyafrica.utils

import android.content.Context
import com.afollestad.materialdialogs.MaterialDialog
import com.simfyafrica.R


class DialogUtil(private val context: Context) {
    var mContext: Context? = null

    init {
        mContext = context
    }

    fun showDialog(message: String) {
        mContext?.let {
            MaterialDialog.Builder(mContext!!)
                    .content(message)
                    .positiveText(R.string.ok)
                    .negativeText(R.string.cancel)
                    .show()
        }
    }

}