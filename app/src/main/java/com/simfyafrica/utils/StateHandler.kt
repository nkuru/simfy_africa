package com.simfyafrica.utils

import android.app.Activity
import java.util.*

class StateHandler {


    private val queueBuffer = Collections.synchronizedList(ArrayList<Runnable>())


    private var activity: Activity? = null


    @Synchronized
    fun resume(activity: Activity) {
        this.activity = activity

        while (queueBuffer.size > 0) {
            val runnable = queueBuffer[0]
            queueBuffer.removeAt(0)
            runnable.run()
        }
    }


    @Synchronized
    fun pause() {
        activity = null
    }


    @Synchronized
    fun run(runnable: Runnable) {
        if (activity == null) {
            queueBuffer.add(runnable)
        } else {
            runnable.run()
        }
    }
}