package com.simfyafrica.inject.component.activity

import com.simfyafrica.inject.module.activity.LoginActivityPresenterModule
import com.simfyafrica.ui.login.activity.view.LoginActivity
import dagger.Subcomponent


@Subcomponent(modules = [(LoginActivityPresenterModule::class)])
interface LoginActivitySubComponent {

    fun inject(activity: LoginActivity)
}