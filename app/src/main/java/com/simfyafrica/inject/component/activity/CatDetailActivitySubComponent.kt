package com.simfyafrica.inject.component.activity

import com.simfyafrica.inject.module.activity.CatDetailActivityPresenterModule
import com.simfyafrica.ui.catdetail.activity.view.CatDetailActivity
import dagger.Subcomponent


@Subcomponent(modules = [(CatDetailActivityPresenterModule::class)])
interface CatDetailActivitySubComponent {

    fun inject(activity: CatDetailActivity)
}