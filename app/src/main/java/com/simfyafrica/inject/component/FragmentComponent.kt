package com.simfyafrica.inject.component

import com.simfyafrica.inject.component.fragment.*
import com.simfyafrica.inject.module.fragment.*
import com.simfyafrica.inject.scope.FragmentScope
import dagger.Component


@FragmentScope
@Component(dependencies = [(AppComponent::class)])
interface FragmentComponent {

    fun plus(module: MainFragmentPresenterModule): MainFragmentSubComponent

    fun plus(module: LoginFragmentPresenterModule): LoginFragmentSubComponent

    fun plus(module: PlaceHoldFragmentPresenterModule): PlaceHoldFragmentSubComponent

    fun plus(module: CatListFragmentPresenterModule): CatListFragmentSubComponent

    fun plus(module: CatDetailFragmentPresenterModule): CatDetailFragmentSubComponent

    fun plus(module: Task3FragmentPresenterModule): Task3FragmentSubComponent

}
