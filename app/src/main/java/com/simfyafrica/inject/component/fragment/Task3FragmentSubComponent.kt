package com.simfyafrica.inject.component.fragment

import com.simfyafrica.inject.module.fragment.Task3FragmentPresenterModule
import com.simfyafrica.ui.task3.Task3Fragment
import dagger.Subcomponent


@Subcomponent(modules = [(Task3FragmentPresenterModule::class)])
interface Task3FragmentSubComponent {

    fun inject(fragment: Task3Fragment)

}