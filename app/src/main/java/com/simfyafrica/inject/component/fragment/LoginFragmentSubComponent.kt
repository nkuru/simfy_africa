package com.simfyafrica.inject.component.fragment

import com.simfyafrica.inject.module.fragment.LoginFragmentPresenterModule
import com.simfyafrica.ui.login.fragment.view.LoginFragment
import dagger.Subcomponent


@Subcomponent(modules = [(LoginFragmentPresenterModule::class)])
interface LoginFragmentSubComponent {

    fun inject(fragment: LoginFragment)

}