package com.simfyafrica.inject.component.fragment

import com.simfyafrica.inject.module.fragment.MainFragmentPresenterModule
import com.simfyafrica.ui.main.fragment.view.MainFragment
import dagger.Subcomponent


@Subcomponent(modules = [(MainFragmentPresenterModule::class)])
interface MainFragmentSubComponent {
    fun inject(fragment: MainFragment)
}