package com.simfyafrica.inject.component

import com.simfyafrica.app.SimfyAfricaApp
import com.simfyafrica.inject.module.ApiModule
import com.simfyafrica.inject.module.AppModule
import com.simfyafrica.inject.module.DatabaseModule
import com.simfyafrica.inject.module.PreferencesModule
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [(AppModule::class), (PreferencesModule::class), (ApiModule::class), (DatabaseModule::class)])
interface AppComponent : IAppComponent {
    fun inject(simfyAfricaApp: SimfyAfricaApp): SimfyAfricaApp

}
