package com.simfyafrica.inject.component.activity

import com.simfyafrica.inject.module.activity.Task3ActivityPresenterModule
import com.simfyafrica.ui.task3.Task3Activity
import dagger.Subcomponent


@Subcomponent(modules = [(Task3ActivityPresenterModule::class)])
interface Task3ActivitySubComponent {

    fun inject(activity: Task3Activity)
}