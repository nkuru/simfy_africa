package com.simfyafrica.inject.component.fragment

import com.simfyafrica.inject.module.fragment.CatListFragmentPresenterModule
import com.simfyafrica.ui.catapi.fragment.view.CatListFragment
import dagger.Subcomponent


@Subcomponent(modules = [(CatListFragmentPresenterModule::class)])
interface CatListFragmentSubComponent {

    fun inject(fragment: CatListFragment)

}