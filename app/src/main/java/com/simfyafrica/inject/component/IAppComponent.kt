package com.simfyafrica.inject.component

import android.content.Context
import com.google.gson.Gson
import com.simfyafrica.database.AppDatabase
import com.simfyafrica.pref.IPreferencesManager
import retrofit2.Retrofit
import javax.inject.Named

interface IAppComponent {

    fun provideContext(): Context

    fun providePreferencesManager(): IPreferencesManager

    fun provideDatabase(): AppDatabase

    @Named("catApi")
    fun provideCatApi(): Retrofit

    @Named("pokeApi")
    fun providePokeApi(): Retrofit

    fun provideGson(): Gson


}
