package com.simfyafrica.inject.component.fragment

import com.simfyafrica.inject.module.fragment.CatDetailFragmentPresenterModule
import com.simfyafrica.ui.catdetail.fragment.view.CatDetailFragment
import dagger.Subcomponent


@Subcomponent(modules = [(CatDetailFragmentPresenterModule::class)])
interface CatDetailFragmentSubComponent {

    fun inject(fragment: CatDetailFragment)

}