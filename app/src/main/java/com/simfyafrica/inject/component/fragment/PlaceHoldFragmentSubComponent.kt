package com.simfyafrica.inject.component.fragment

import com.simfyafrica.inject.module.fragment.PlaceHoldFragmentPresenterModule
import com.simfyafrica.ui.placehold.fragment.view.PlaceHoldFragment
import dagger.Subcomponent


@Subcomponent(modules = [(PlaceHoldFragmentPresenterModule::class)])
interface PlaceHoldFragmentSubComponent {

    fun inject(fragment: PlaceHoldFragment)

}