package com.simfyafrica.inject.component.activity

import com.simfyafrica.inject.module.activity.CatListActivityPresenterModule
import com.simfyafrica.ui.catapi.activity.view.CatListActivity
import dagger.Subcomponent


@Subcomponent(modules = [(CatListActivityPresenterModule::class)])
interface CatListActivitySubComponent {

    fun inject(activity: CatListActivity)
}