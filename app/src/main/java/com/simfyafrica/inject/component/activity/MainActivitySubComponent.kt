package com.simfyafrica.inject.component.activity

import com.simfyafrica.inject.module.activity.MainActivityPresenterModule
import com.simfyafrica.ui.main.activity.view.MainActivity
import dagger.Subcomponent


@Subcomponent(modules = [(MainActivityPresenterModule::class)])
interface MainActivitySubComponent {

    fun inject(activity: MainActivity)
}