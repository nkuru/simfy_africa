package com.simfyafrica.inject.component

import com.simfyafrica.inject.component.activity.*
import com.simfyafrica.inject.module.activity.*
import com.simfyafrica.inject.scope.ActivityScope
import dagger.Component

@ActivityScope
@Component(dependencies = arrayOf(AppComponent::class),
        modules = [(BaseComponentActivityModule::class)])
interface ActivityComponent : AppComponent {

    operator fun plus(module: MainActivityPresenterModule): MainActivitySubComponent

    operator fun plus(module: LoginActivityPresenterModule): LoginActivitySubComponent

    operator fun plus(module: PlaceHoldActivityPresenterModule): PlaceHoldActivitySubComponent

    operator fun plus(module: CatListActivityPresenterModule): CatListActivitySubComponent

    operator fun plus(module: CatDetailActivityPresenterModule): CatDetailActivitySubComponent

    operator fun plus(module: Task3ActivityPresenterModule): Task3ActivitySubComponent


}
