package com.simfyafrica.inject.component.activity

import com.simfyafrica.inject.module.activity.PlaceHoldActivityPresenterModule
import com.simfyafrica.ui.placehold.activity.view.PlaceholdActivity
import dagger.Subcomponent


@Subcomponent(modules = [(PlaceHoldActivityPresenterModule::class)])
interface PlaceHoldActivitySubComponent {

    fun inject(activity: PlaceholdActivity)
}