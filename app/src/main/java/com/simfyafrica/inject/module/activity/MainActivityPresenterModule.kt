package com.simfyafrica.inject.module.activity

import com.simfyafrica.pref.IPreferencesManager
import com.simfyafrica.ui.main.activity.presenter.IMainActivityPresenter
import com.simfyafrica.ui.main.activity.presenter.MainActivityPresenter
import dagger.Module
import dagger.Provides


@Module
class MainActivityPresenterModule {

    @Provides
    internal fun providePresenter(
            preferencesManager: IPreferencesManager): IMainActivityPresenter = MainActivityPresenter(preferencesManager)

}

