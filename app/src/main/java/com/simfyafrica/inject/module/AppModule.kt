package com.simfyafrica.inject.module

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class AppModule(private val application: Application) {

    var mApplication:Application?=null

    init {
        this.mApplication=application
    }

    @Singleton
    @Provides
    fun provideContext(): Context = mApplication!!

    @Singleton
    @Provides
    fun provideApplication(): Application = mApplication!!

    /*@Binds
    @Singleton
    fun bindApplication(): Application = mApplication!!*/

}
