package com.simfyafrica.inject.module.fragment

import com.simfyafrica.database.AppDatabase
import com.simfyafrica.ui.catapi.fragment.presenter.CatListFragmentPresenter
import com.simfyafrica.ui.catapi.fragment.presenter.ICatListFragmentPresenter
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Named


@Module
class CatListFragmentPresenterModule {

    @Provides
    internal fun providePresenter(appDatabase: AppDatabase, @Named("catApi")retrofit: Retrofit): ICatListFragmentPresenter = CatListFragmentPresenter(appDatabase, retrofit)

}