package com.simfyafrica.inject.module.activity

import com.simfyafrica.ui.placehold.activity.presenter.IPlaceholdActivityPresenter
import com.simfyafrica.ui.placehold.activity.presenter.PlaceHoldActivityPresenter
import dagger.Module
import dagger.Provides


@Module
class PlaceHoldActivityPresenterModule {

    @Provides
    internal fun providePresenter(): IPlaceholdActivityPresenter = PlaceHoldActivityPresenter()

}

