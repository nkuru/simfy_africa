package com.simfyafrica.inject.module.activity

import com.simfyafrica.database.AppDatabase
import com.simfyafrica.pref.IPreferencesManager
import com.simfyafrica.ui.base.activity.component.presenter.BaseActivityPresenter
import com.simfyafrica.ui.base.activity.component.presenter.IBaseActivityPresenter
import dagger.Module
import dagger.Provides

@Module
class BaseComponentActivityModule {

    @Provides
    internal fun provideBaseActivityPresenter(preferencesManager: IPreferencesManager,
                                              database: AppDatabase)
            : IBaseActivityPresenter = BaseActivityPresenter(preferencesManager, database)

}
