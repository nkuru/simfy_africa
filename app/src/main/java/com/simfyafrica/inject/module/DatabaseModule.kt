package com.simfyafrica.inject.module

import android.arch.persistence.room.Room
import android.content.Context
import com.simfyafrica.R
import com.simfyafrica.database.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
open class DatabaseModule {


    //FIXME -- I did not want to create an asynctask or background thread so I used allowMainThreadQueries(). In a real world this should not be allowed
    @Provides
    @Singleton
    open fun provideDB(context: Context): AppDatabase = Room.databaseBuilder(context,
            AppDatabase::class.java, context.getString(R.string.DATABASE_NAME)).allowMainThreadQueries().build()


}
