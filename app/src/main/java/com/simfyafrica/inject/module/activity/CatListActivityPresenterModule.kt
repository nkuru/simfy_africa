package com.simfyafrica.inject.module.activity

import com.simfyafrica.ui.catapi.activity.presenter.CatListActivityPresenter
import com.simfyafrica.ui.catapi.activity.presenter.ICatListAtivityPresenter
import dagger.Module
import dagger.Provides


@Module
class CatListActivityPresenterModule {

    @Provides
    internal fun providePresenter(): ICatListAtivityPresenter = CatListActivityPresenter()

}

