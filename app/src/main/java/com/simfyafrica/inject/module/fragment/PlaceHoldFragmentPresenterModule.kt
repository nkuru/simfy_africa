package com.simfyafrica.inject.module.fragment

import com.simfyafrica.pref.IPreferencesManager
import com.simfyafrica.ui.placehold.fragment.presenter.IPlaceholdFragmentPresenter
import com.simfyafrica.ui.placehold.fragment.presenter.PlaceHoldFragmentPresenter
import dagger.Module
import dagger.Provides


@Module
class PlaceHoldFragmentPresenterModule {

    @Provides
    internal fun providePresenter(preferencesManager: IPreferencesManager): IPlaceholdFragmentPresenter = PlaceHoldFragmentPresenter(preferencesManager)
}