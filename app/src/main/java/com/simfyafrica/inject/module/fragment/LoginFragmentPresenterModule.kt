package com.simfyafrica.inject.module.fragment

import com.simfyafrica.pref.IPreferencesManager
import com.simfyafrica.ui.login.fragment.presenter.ILoginFragmentPresenter
import com.simfyafrica.ui.login.fragment.presenter.LoginFragmentPresenter
import dagger.Module
import dagger.Provides


@Module
class LoginFragmentPresenterModule {

    @Provides
    internal fun providePresenter(preferencesManager: IPreferencesManager): ILoginFragmentPresenter = LoginFragmentPresenter(preferencesManager)
}