package com.simfyafrica.inject.module.activity

import com.simfyafrica.ui.login.activity.presenter.ILoginActivityPresenter
import com.simfyafrica.ui.login.activity.presenter.LoginActivityPresenter
import dagger.Module
import dagger.Provides


@Module
class LoginActivityPresenterModule {

    @Provides
    internal fun providePresenter(): ILoginActivityPresenter = LoginActivityPresenter()

}

