package com.simfyafrica.inject.module

import android.content.Context
import com.google.gson.Gson
import com.simfyafrica.pref.IPreferencesManager
import com.simfyafrica.pref.Preference
import com.simfyafrica.pref.PreferencesManager
import dagger.Module
import dagger.Provides

@Module
open class PreferencesModule {

    @Provides
    open fun providePreferencesManager(context: Context): IPreferencesManager =
            PreferencesManager(Preference(context), Gson())
}
