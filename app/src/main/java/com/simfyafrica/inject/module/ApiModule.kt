package com.simfyafrica.inject.module

import android.app.Application
import android.content.Context
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.simfyafrica.R
import com.simfyafrica.api.RetrofitService
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.simpleframework.xml.convert.AnnotationStrategy
import org.simpleframework.xml.core.Persister
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import javax.inject.Named
import javax.inject.Singleton


@Module
open class ApiModule {


//    @Provides
//    @Singleton
//    open fun provideRetrofitService(context: Context, retrofit: Retrofit): RetrofitService = retrofit.create(RetrofitService::class.java)


    @Provides
    @Singleton
    fun provideHttpCache(application: Application): Cache {
        val cacheSize = 10 * 1024 * 1024 * 1L
        return Cache(application.cacheDir, cacheSize)
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        return gsonBuilder.create()
    }

    @Provides
    @Singleton
    fun provideOkhttpClient(cache: Cache): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder().addInterceptor(interceptor)
        client.cache(cache)
        return client.build()
    }

    @Provides @Named("catApi")
    @Singleton
    fun provideCatApiRetrofit(okHttpClient: OkHttpClient, context: Context): Retrofit {

        return Retrofit.Builder()
                .addConverterFactory(SimpleXmlConverterFactory.createNonStrict(
                        Persister(AnnotationStrategy())))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(context.getString(R.string.CAT_API_URL))
                .client(okHttpClient)
                .build()
    }

    @Provides @Named("pokeApi")
    @Singleton
    fun providePokeApiRetrofit(okHttpClient: OkHttpClient, context: Context): Retrofit {

        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(context.getString(R.string.POKE_API_URL))
                .client(okHttpClient)
                .build()
    }
}
