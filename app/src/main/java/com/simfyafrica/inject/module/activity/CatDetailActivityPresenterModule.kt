package com.simfyafrica.inject.module.activity

import com.simfyafrica.ui.catdetail.activity.presenter.CatDetailActivityPresenter
import com.simfyafrica.ui.catdetail.activity.presenter.ICatDetailActivityPresenter
import dagger.Module
import dagger.Provides


@Module
class CatDetailActivityPresenterModule {

    @Provides
    internal fun providePresenter(): ICatDetailActivityPresenter = CatDetailActivityPresenter()

}

