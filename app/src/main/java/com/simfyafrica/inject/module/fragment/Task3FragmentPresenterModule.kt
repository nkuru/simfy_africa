package com.simfyafrica.inject.module.fragment

import com.google.gson.Gson
import com.simfyafrica.database.AppDatabase
import com.simfyafrica.ui.task3.ITask3FragmentPresenter
import com.simfyafrica.ui.task3.Task3FragmentPresenter
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named


@Module
class Task3FragmentPresenterModule {

    @Provides
    internal fun providePresenter(appDatabase: AppDatabase, @Named("pokeApi")retrofit: Retrofit, gson: Gson): ITask3FragmentPresenter = Task3FragmentPresenter(appDatabase, retrofit, gson)

}