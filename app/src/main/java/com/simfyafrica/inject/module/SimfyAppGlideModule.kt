package com.simfyafrica.inject.module

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class SimfyAppGlideModule :AppGlideModule(){
}