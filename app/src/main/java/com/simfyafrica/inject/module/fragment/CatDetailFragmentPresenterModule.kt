package com.simfyafrica.inject.module.fragment

import com.simfyafrica.ui.catdetail.fragment.presenter.CatDetailFragmentPresenter
import com.simfyafrica.ui.catdetail.fragment.presenter.ICatDetailFragmentPresenter
import dagger.Module
import dagger.Provides


@Module
class CatDetailFragmentPresenterModule {

    @Provides
    internal fun providePresenter(): ICatDetailFragmentPresenter = CatDetailFragmentPresenter()

}