package com.simfyafrica.inject.module.fragment

import com.simfyafrica.database.AppDatabase
import com.simfyafrica.pref.IPreferencesManager
import com.simfyafrica.ui.main.fragment.presenter.IMainFragmentPresenter
import com.simfyafrica.ui.main.fragment.presenter.MainFragmentPresenter
import dagger.Module
import dagger.Provides


@Module
class MainFragmentPresenterModule {

    @Provides
    internal fun providePresenter(preferencesManager: IPreferencesManager, appDatabase: AppDatabase): IMainFragmentPresenter = MainFragmentPresenter(preferencesManager, appDatabase)
}