package com.simfyafrica.inject.module.activity

import com.simfyafrica.ui.task3.ITask3ActivityPresenter
import com.simfyafrica.ui.task3.Task3ActivityPresenter
import dagger.Module
import dagger.Provides


@Module
class Task3ActivityPresenterModule {

    @Provides
    internal fun providePresenter(): ITask3ActivityPresenter = Task3ActivityPresenter()

}

