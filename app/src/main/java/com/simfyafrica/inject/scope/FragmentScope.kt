package com.simfyafrica.inject.scope

@javax.inject.Scope
@kotlin.annotation.Retention
annotation class FragmentScope
