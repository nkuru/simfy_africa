package com.simfyafrica.pref


interface IPreferencesManager {

    fun clearData()
    val getUsername: String
    fun setUsername(value: String?)
}