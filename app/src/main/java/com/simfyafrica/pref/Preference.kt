package com.simfyafrica.pref

import android.annotation.SuppressLint
import android.content.Context
import com.ironz.binaryprefs.BinaryPreferencesBuilder
import com.simfyafrica.BuildConfig

class Preference(val context: Context) {

    private val prefs: com.ironz.binaryprefs.Preferences = BinaryPreferencesBuilder(context)
            .name(BuildConfig.APPLICATION_ID)
            .build()

    fun commitClear() = {
        prefs.edit().clear().commit()
    }

    @Suppress("UNCHECKED_CAST")
    fun <T> loadPreference(name: String, default: T): T = with(prefs) {
        val res: Any = when (default) {
            is Long -> getLong(name, default)
            is String -> getString(name, default)
            is Int -> getInt(name, default)
            is Boolean -> getBoolean(name, default)
            is Float -> getFloat(name, default)
            else -> throw IllegalArgumentException("This type cannot be saved into Preferences")
        }

        res as T
    }

    @SuppressLint("CommitPrefEdits")
    fun <T> applyPreference(name: String, value: T) = with(prefs.edit()) {
        when (value) {
            is Long -> putLong(name, value)
            is String -> putString(name, value)
            is Int -> putInt(name, value)
            is Boolean -> putBoolean(name, value)
            is Float -> putFloat(name, value)
            else -> throw IllegalArgumentException(
                    "This type can't be saved into Preferences")
        }.apply()
    }

}
