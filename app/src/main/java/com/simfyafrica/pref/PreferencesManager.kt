package com.simfyafrica.pref

import com.google.gson.Gson


class PreferencesManager(private val preferences: Preference,
                         private val gson: Gson) : IPreferencesManager {


    override fun clearData() {
        preferences.commitClear()
    }


    override val getUsername: String
        get() = preferences.loadPreference(KEY_USERNAME, "")


    override fun setUsername(value: String?) {
        value?.let {
            preferences.applyPreference(KEY_USERNAME, it)
        }
    }


    companion object {

        private const val KEY_USERNAME = "KEY_USERNAME"
    }

}
