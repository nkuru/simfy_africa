package com.simfyafrica.api


class ApiException(error: String) : Exception() {

    companion object {

        val CODE_NO_INTERNET_CONNECTION = 1000

    }
}
