package com.simfyafrica.api

import com.simfyafrica.models.PokeResponse
import com.simfyafrica.models.Response
import retrofit2.http.GET
import retrofit2.http.Path
import rx.Observable

interface RetrofitService {

    @GET("images/get?format=xml&results_per_page=100&size=small&type=png")
    fun getImages(): Observable<Response>

    @GET("v2/pokemon/{id}")
    fun getPokeResponse(@Path("id") id: String): Observable<PokeResponse>
}