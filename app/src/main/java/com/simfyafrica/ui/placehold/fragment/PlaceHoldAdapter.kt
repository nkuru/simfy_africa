package com.simfyafrica.ui.placehold.fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.simfyafrica.R
import com.simfyafrica.inject.module.GlideApp
import com.simfyafrica.ui.base.adapter.BaseAdapter
import kotlinx.android.synthetic.main.placehold_item.view.*

class PlaceHoldAdapter(data: MutableList<Int>) : BaseAdapter<Int, BaseAdapter.ViewHolder>(data) {


    override fun onBindViewHolder(holder: BaseAdapter.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(mData[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val holder = LayoutInflater.from(parent.context)
                .inflate(R.layout.placehold_item, parent, false) as View
        return ViewHolder(holder)
    }

    class ViewHolder(holder: View) : BaseAdapter.ViewHolder(holder, null) {

        fun bind(item: Int) {
            itemView.tvTitle.text="Item ${item+1}"
            itemView.tvDescription.text="This is the description for Item ${item+1}"
            GlideApp.with(this.itemView)
                    .load("http://placehold.it/2048&text=Item ${item+1}")
                    .placeholder(R.drawable.placeholder)
                    .into(itemView.ivImage)
        }
    }
}
