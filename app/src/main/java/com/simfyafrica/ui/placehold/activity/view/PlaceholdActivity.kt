package com.simfyafrica.ui.placehold.activity.view

import android.os.Bundle
import android.view.MenuItem
import com.simfyafrica.R
import com.simfyafrica.inject.component.ActivityComponent
import com.simfyafrica.inject.module.activity.PlaceHoldActivityPresenterModule
import com.simfyafrica.ui.base.activity.component.BaseComponentActivity
import com.simfyafrica.ui.placehold.activity.presenter.IPlaceholdActivityPresenter
import com.simfyafrica.ui.placehold.fragment.view.PlaceHoldFragment
import javax.inject.Inject

class PlaceholdActivity : BaseComponentActivity(), IPlaceholdActivityView {

    @Inject
    lateinit var presenter: IPlaceholdActivityPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_placehold)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        afterViews()
    }

    private fun afterViews() {
        switchFragment(PlaceHoldFragment.newInstance(), true, false, true)
    }

    override fun inject(activityComponent: ActivityComponent) {
        activityComponent.plus(PlaceHoldActivityPresenterModule()).inject(this)
        registerPresenter(presenter, this)
    }

    override fun logAction(name: String) {
        //todo --
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showDialog(message: String) {
        //todo
    }

}
