package com.simfyafrica.ui.placehold.fragment.view

import com.simfyafrica.ui.base.IView

interface IPlaceHoldFragmentView: IView{
    fun showUsername(username: String)
}