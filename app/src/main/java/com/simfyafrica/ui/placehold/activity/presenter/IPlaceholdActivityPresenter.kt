package com.simfyafrica.ui.placehold.activity.presenter

import com.simfyafrica.ui.base.presenter.IPresenter
import com.simfyafrica.ui.placehold.activity.view.IPlaceholdActivityView


interface IPlaceholdActivityPresenter: IPresenter<IPlaceholdActivityView>

//todo -- Add body here