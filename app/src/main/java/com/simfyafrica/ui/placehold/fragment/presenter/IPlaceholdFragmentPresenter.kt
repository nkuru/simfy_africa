package com.simfyafrica.ui.placehold.fragment.presenter

import com.simfyafrica.ui.base.presenter.IPresenter
import com.simfyafrica.ui.placehold.fragment.view.IPlaceHoldFragmentView


interface IPlaceholdFragmentPresenter: IPresenter<IPlaceHoldFragmentView>{

    fun getUsername()
}