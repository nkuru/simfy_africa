package com.simfyafrica.ui.placehold.fragment.presenter

import com.simfyafrica.pref.IPreferencesManager
import com.simfyafrica.ui.base.presenter.RxBasePresenter
import com.simfyafrica.ui.placehold.fragment.view.IPlaceHoldFragmentView


class PlaceHoldFragmentPresenter(private val preferencesManager: IPreferencesManager) :
        RxBasePresenter<IPlaceHoldFragmentView>(), IPlaceholdFragmentPresenter {

    override fun getUsername() {
        getView().showUsername(preferencesManager.getUsername)
    }


}