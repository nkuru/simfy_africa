package com.simfyafrica.ui.placehold.fragment.view

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.simfyafrica.R
import com.simfyafrica.inject.component.FragmentComponent
import com.simfyafrica.inject.module.fragment.PlaceHoldFragmentPresenterModule
import com.simfyafrica.ui.base.fragment.BaseComponentFragment
import com.simfyafrica.ui.placehold.fragment.PlaceHoldAdapter
import com.simfyafrica.ui.placehold.fragment.presenter.IPlaceholdFragmentPresenter
import kotlinx.android.synthetic.main.fragment_placehold.*
import javax.inject.Inject


class PlaceHoldFragment : BaseComponentFragment(), IPlaceHoldFragmentView {

    override fun showUsername(username: String) {
        tvWelcomeUsername.text = getString(R.string.welcome_username, username)
    }

    var mAdapter: PlaceHoldAdapter? = null
    private var mLayoutManager: RecyclerView.LayoutManager? = null

    @Inject
    lateinit var placeHoldFragmentPresenter: IPlaceholdFragmentPresenter


    companion object {
        fun newInstance(): PlaceHoldFragment {
            return PlaceHoldFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(
            R.layout.fragment_placehold, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initializeRecyclerView()
        afterViews()
    }

    private fun afterViews() {
        tvWelcomeUsername.text
        placeHoldFragmentPresenter.getUsername()
    }

    private fun initializeRecyclerView() {
        val items = mutableListOf<Int>()
        for (i in 0..249) {
            items.add(i)
        }
        mAdapter = PlaceHoldAdapter(items)
        mLayoutManager = LinearLayoutManager(activity)
        recyclerView.layoutManager = mLayoutManager
        val dividerItemDecoration = DividerItemDecoration(recyclerView.context,
                LinearLayoutManager.VERTICAL)
        recyclerView.addItemDecoration(dividerItemDecoration)
        recyclerView.adapter = mAdapter

    }


    override fun inject(fragmentComponent: FragmentComponent) {
        fragmentComponent.plus(PlaceHoldFragmentPresenterModule()).inject(this)
        registerPresenter(placeHoldFragmentPresenter, this)
    }

    override fun logAction(name: String) {
        //todo -- log action
    }


}