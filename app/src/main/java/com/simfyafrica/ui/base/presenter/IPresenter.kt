package com.simfyafrica.ui.base.presenter


interface IPresenter<in TPresenterView> : IPresenterCallbacks {
    fun setView(var1: TPresenterView)

    fun showProgressDialog()

    fun hideProgressDialog()

    fun handleError(e: Throwable)
}
