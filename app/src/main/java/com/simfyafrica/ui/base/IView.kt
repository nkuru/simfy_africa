package com.simfyafrica.ui.base

import android.app.Activity

interface IView {

    fun getViewContext(): Activity

    fun handleError(throwable: Throwable)

    fun showProgress()

    fun hideProgress()

    fun logAction(name: String)

    fun showDialog(message: String)
}