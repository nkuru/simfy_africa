package com.simfyafrica.ui.base.adapter

import android.support.v7.widget.RecyclerView
import android.view.View

abstract class BaseAdapter<T, VH : RecyclerView.ViewHolder>(protected var mData: MutableList<T>) : RecyclerView.Adapter<VH>() {

    protected var mOnItemClickListener: OnItemClickListener?=null

    override fun getItemCount(): Int {
        return mData.size
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        mOnItemClickListener = onItemClickListener
    }

    fun clear() {
        mData.clear()
    }

    fun addAll(data: List<T>) {
        mData.addAll(data)
    }

    fun getItem(position: Int): T {
        return mData[position]
    }

    open class ViewHolder(itemView: View, private val mOnItemClickListener: OnItemClickListener?) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            mOnItemClickListener?.onItemClick(v, adapterPosition)
        }
    }
}
