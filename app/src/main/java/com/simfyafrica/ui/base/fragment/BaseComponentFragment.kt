package com.simfyafrica.ui.base.fragment

import android.os.Bundle
import com.simfyafrica.app.SimfyAfricaApp
import com.simfyafrica.inject.component.AppComponent
import com.simfyafrica.inject.component.DaggerFragmentComponent
import com.simfyafrica.inject.component.FragmentComponent
import com.simfyafrica.ui.base.IView
import com.simfyafrica.ui.base.activity.component.BaseComponentActivity
import com.simfyafrica.utils.DialogUtil
import com.simfyafrica.utils.ProgressDialogHelper

abstract class BaseComponentFragment : BaseFragment(), IView {

    private var progressDialog: ProgressDialogHelper? = null
    private var dialogUtil: DialogUtil?=null


    override fun getViewContext() = this.activity!!

    init {
        inject(injectComponent())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        progressDialog = context?.let { ProgressDialogHelper(it) }
        dialogUtil = context?.let { DialogUtil(it) }
    }


    override fun onDestroy() {
        super.onDestroy()
        progressDialog?.dismissDialog()
    }

    override fun showProgress() {
        progressDialog?.showProgressDialog()
    }


    override fun hideProgress() {
        progressDialog?.dismissDialog()
    }


    override fun showDialog(message:String){
        dialogUtil?.showDialog(message)
    }



    override fun handleError(throwable: Throwable) {
        val activity = activity
        if (activity is BaseComponentActivity) {
            (getActivity() as BaseComponentActivity).handleError(throwable, this)
        } else {
            super.handleError(throwable)
        }
    }


    abstract fun inject(fragmentComponent: FragmentComponent)

    private fun injectComponent(): FragmentComponent = DaggerFragmentComponent
            .builder()
            .appComponent(SimfyAfricaApp.application.appComponent as AppComponent)
            .build()
}
