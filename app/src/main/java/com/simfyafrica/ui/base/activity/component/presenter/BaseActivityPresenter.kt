package com.simfyafrica.ui.base.activity.component.presenter

import com.simfyafrica.database.AppDatabase
import com.simfyafrica.pref.IPreferencesManager
import com.simfyafrica.ui.base.activity.component.IBaseComponentActivityView
import com.simfyafrica.ui.base.presenter.RxBasePresenter

class BaseActivityPresenter(private val preferencesManager: IPreferencesManager, appDatabase: AppDatabase) :
        RxBasePresenter<IBaseComponentActivityView>(), IBaseActivityPresenter
