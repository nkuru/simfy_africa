package com.simfyafrica.ui.base.activity.component

import com.simfyafrica.ui.base.activity.component.presenter.IBaseActivityPresenter
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import com.simfyafrica.R
import com.simfyafrica.app.SimfyAfricaApp
import com.simfyafrica.inject.component.ActivityComponent
import com.simfyafrica.inject.component.AppComponent
import com.simfyafrica.inject.component.DaggerActivityComponent
import com.simfyafrica.ui.base.IView
import com.simfyafrica.ui.base.activity.BaseActivity
import com.simfyafrica.utils.ProgressDialogHelper
import com.simfyafrica.utils.StateHandler
import javax.inject.Inject

abstract class BaseComponentActivity : BaseActivity(), IView, IBaseComponentActivityView {

    @Inject
    lateinit var baseActivityPresenter: IBaseActivityPresenter

    override fun getViewContext(): Activity = this

    private var stateHandler = StateHandler()

    private var progressDialog: ProgressDialogHelper? = null

    init {
        @Suppress("LeakingThis")
        inject(injectComponent())
        registerPresenter(baseActivityPresenter, this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        progressDialog = ProgressDialogHelper(this)
    }


    override fun onDestroy() {
        super.onDestroy()
        progressDialog?.dismissDialog()
    }

    override fun onPause() {
        super.onPause()
        stateHandler.pause()
    }

    override fun onPostResume() {
        super.onPostResume()
        stateHandler.resume(this)
    }

    override fun handleError(throwable: Throwable) {
        if (handleApiException(throwable, null)) {
            return
        }
        super.handleError(throwable)
    }

    fun handleError(throwable: Throwable, fragment: Fragment) {
        if (handleApiException(throwable, fragment)) {
            return
        }
        super.handleError(throwable)
    }

    override fun showProgress() {
        progressDialog?.showProgressDialog()
    }

    override fun hideProgress() {
        progressDialog?.dismissDialog()
    }

    abstract fun inject(activityComponent: ActivityComponent)

    private fun injectComponent(): ActivityComponent = DaggerActivityComponent
            .builder()
            .appComponent(SimfyAfricaApp.application.appComponent as AppComponent)
            .build()

    private fun handleApiException(throwable: Throwable, fragment: Fragment?): Boolean {
        //TODO
        return false
    }

    protected fun showNoInternetConnection() {
        //TODO
    }

    fun switchFragment(fragment: Fragment, withAnimation: Boolean, addToBackStack: Boolean,
                       clearBackStack: Boolean) {
        val fragmentManager = supportFragmentManager
        if (clearBackStack) {
            fragmentManager.popBackStackImmediate(null,
                    FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }
        val fragmentTransaction = fragmentManager.beginTransaction()
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(fragment.javaClass.simpleName)
        }
        if (withAnimation) {
            fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                    android.R.anim.fade_out,
                    android.R.anim.fade_in, android.R.anim.fade_out)
        }
        fragmentTransaction
                .replace(R.id.content, fragment, fragment.javaClass.simpleName)
        fragmentTransaction.commitAllowingStateLoss()
    }



}
