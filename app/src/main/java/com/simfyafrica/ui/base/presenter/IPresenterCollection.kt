package com.simfyafrica.ui.base.presenter


interface IPresenterCollection {
    fun <TPresenter : IPresenter<TPresenterView>, TPresenterView> registerPresenter(
            presenter: TPresenter, presenterView: TPresenterView): TPresenter
}