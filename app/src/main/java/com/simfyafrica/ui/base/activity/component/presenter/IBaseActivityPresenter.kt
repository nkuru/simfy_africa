package com.simfyafrica.ui.base.activity.component.presenter

import com.simfyafrica.ui.base.activity.component.IBaseComponentActivityView
import com.simfyafrica.ui.base.presenter.IPresenter

interface IBaseActivityPresenter : IPresenter<IBaseComponentActivityView>
