package com.simfyafrica.ui.base.presenter

import android.app.Activity
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import com.simfyafrica.ui.base.IView
import rx.Observable
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.functions.Action0
import rx.subscriptions.CompositeSubscription

abstract class RxBasePresenter<TPresenterView : IView> : BasePresenter<TPresenterView>() {

    private var compositeSubscription: CompositeSubscription? = null

    init {
        compositeSubscription = CompositeSubscription()
    }

    override fun onDestroy() {
        clear()
        compositeSubscription = null
    }

    protected fun clear() {
        compositeSubscription?.clear()
    }

    protected fun addSubscriber(subscription: Subscription) {
        compositeSubscription?.add(subscription)
    }

    protected fun <T> applyProgress(beforeRun: () -> Unit = {},
                                    afterRun: () -> Unit = {}): Observable.Transformer<T, T> = Observable.Transformer {
        it.observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(beforeRun)
                .doOnTerminate(afterRun)
    }

    protected fun <T> applyProgress(beforeRun: Action0?,
                                    afterRun: Action0?): Observable.Transformer<T, T> = Observable.Transformer {
        it.observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(beforeRun ?: Action0 { getView().showProgress() })
                .doOnTerminate(afterRun ?: Action0 { getView().hideProgress() })
    }

    /**
     * You must call this method just before subscribe, to send control to main thread
     */
    fun <T> applyProgress(): Observable.Transformer<T, T> = applyProgress(
            Action0 { this.showProgressDialog() }, Action0 { this.hideProgressDialog() })

    companion object {

        const val GRANTED = 1

        const val DENIED = 2

        const val BLOCKED_OR_NEVER_ASK = 3

        fun getPermissionStatus(activity: Activity, androidPermissionName: String): Int {
            if (ContextCompat.checkSelfPermission(activity,
                            androidPermissionName) != PackageManager.PERMISSION_GRANTED) {
                if (!ActivityCompat
                                .shouldShowRequestPermissionRationale(activity, androidPermissionName)) {
                    return BLOCKED_OR_NEVER_ASK
                }
                return DENIED
            }
            return GRANTED
        }
    }

}