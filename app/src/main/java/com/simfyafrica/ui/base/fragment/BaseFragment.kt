package com.simfyafrica.ui.base.fragment

import android.content.Context
import android.net.ConnectivityManager
import com.simfyafrica.ui.base.activity.BaseActivity

abstract class BaseFragment : BaseMvpFragment() {

    private val baseActivity: BaseActivity?
        get() {
            val activity = activity ?: return null
            return if (activity is BaseActivity) {
                activity
            } else {
                throw IllegalStateException("BaseFragment can be used only in base activity! Current fragment instance of: " + this.javaClass.simpleName + " Current activity instance of: " + getActivity().toString())
            }
        }

    open fun handleError(throwable: Throwable) {
        val baseActivity = baseActivity
        baseActivity?.handleError(throwable)
    }

    internal fun isConnectedOrConnecting(context: Context): Boolean {
        val connMgr = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnectedOrConnecting
    }

}
