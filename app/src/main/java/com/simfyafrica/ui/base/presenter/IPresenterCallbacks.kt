package com.simfyafrica.ui.base.presenter

import android.content.Intent
import android.os.Bundle

interface IPresenterCallbacks {
    fun onCreate(var1: Bundle?)

    fun onStart()

    fun onResume()

    fun onPause()

    fun onStop()

    fun onDestroy()

    fun onSaveInstanceState(outState: Bundle)

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean
}
