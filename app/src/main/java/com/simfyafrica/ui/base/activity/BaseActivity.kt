package com.simfyafrica.ui.base.activity


import android.content.Context
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.View
import com.simfyafrica.BuildConfig
import com.simfyafrica.R
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper


abstract class BaseActivity : BaseMvpActivity() {


    override fun attachBaseContext(newBase: Context) = super.attachBaseContext(
            CalligraphyContextWrapper.wrap(newBase))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Log.i("TAG_ACTIVITY", "${this.javaClass.simpleName} onCreate")
    }

    open fun handleError(throwable: Throwable) {
        if (BuildConfig.DEBUG) {
            throwable.printStackTrace()
        }

    }

    protected fun createAlertDialog(message: String,
                                    doOnError: (() -> Unit) = { }): AlertDialog.Builder {
        val builder = AlertDialog.Builder(this, R.style.WarningAlertDialogTheme)
        return builder.setTitle(getString(R.string.warning))
                .setMessage(message)
                .setOnDismissListener {
                    doOnError()
                }
//                .setPositiveButton(R.string.restart, { _, _ -> restartApp() })

        //FIXME -- Fix positive button message and listener
    }




    internal fun hideSystemUI() {
        val decorView = window.decorView

        decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                // Set the content to appear under the system bars so that the
                // content doesn't resize when the system bars hide and show.
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                // Hide the nav bar and status bar
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN)
    }
}
