package com.simfyafrica.ui.base.adapter

import android.view.View

interface OnItemClickListener {
    fun onItemClick(view: View, position: Int)
}
