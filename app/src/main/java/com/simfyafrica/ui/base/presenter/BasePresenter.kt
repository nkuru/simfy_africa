package com.simfyafrica.ui.base.presenter


import android.content.Intent
import android.os.Bundle
import com.simfyafrica.ui.base.IView

abstract class BasePresenter<TPresenterView : IView> internal constructor() :
        IPresenter<TPresenterView>, IPresenterCollection {

    private var presenterView: TPresenterView? = null

    private val presenterCollection = PresenterCollection()

    fun getView(): TPresenterView = this.presenterView!!

    override fun setView(var1: TPresenterView) = if (this.presenterView != null) {
        throw RuntimeException("Cannot set presenterView twice")
    } else {
        this.presenterView = var1
    }

    override fun showProgressDialog() = getView().showProgress()

    override fun hideProgressDialog() = getView().hideProgress()

    open val handleError: (Throwable) -> Unit = {
        handleError(it)
    }

    override fun handleError(e: Throwable) = getView().handleError(e)

    override fun onCreate(var1: Bundle?) = if (this.presenterView == null) {
        throw NullPointerException("presenterView cannot to be null")
    } else {
        this.presenterCollection.onCreate(var1)
    }

    override fun onStart() = this.presenterCollection.onStart()

    override fun onResume() = this.presenterCollection.onResume()

    override fun onPause() = this.presenterCollection.onPause()

    override fun onStop() = this.presenterCollection.onStop()

    override fun onDestroy() = this.presenterCollection.onDestroy()

    override fun onSaveInstanceState(
            outState: Bundle) = this.presenterCollection.onSaveInstanceState(outState)

    override fun onActivityResult(requestCode: Int, resultCode: Int,
                                  data: Intent?): Boolean = this.presenterCollection.onActivityResult(
            requestCode, resultCode, data)

    override fun <TPresenter : IPresenter<TPresenterView2>, TPresenterView2> registerPresenter(
            presenter: TPresenter,
            presenterView: TPresenterView2): TPresenter = this.presenterCollection.registerPresenter(
            presenter, presenterView)
}
