package com.simfyafrica.ui.base.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.simfyafrica.ui.base.presenter.IPresenter
import com.simfyafrica.ui.base.presenter.IPresenterCollection
import com.simfyafrica.ui.base.presenter.PresenterCollection

abstract class BaseMvpActivity : AppCompatActivity(), IPresenterCollection {

    private val presenterCollection: PresenterCollection = PresenterCollection()

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.presenterCollection.onCreate(savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        this.presenterCollection.onStart()
    }

    override fun onResume() {
        super.onResume()
        this.presenterCollection.onResume()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        this.presenterCollection.onSaveInstanceState(outState)
    }

    override fun onPause() {
        super.onPause()
        this.presenterCollection.onPause()
    }

    override fun onStop() {
        super.onStop()
        this.presenterCollection.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        this.presenterCollection.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!this.presenterCollection.onActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    final override fun <TPresenter : IPresenter<TPresenterView>, TPresenterView> registerPresenter(
            presenter: TPresenter, presenterView: TPresenterView) =
            this.presenterCollection.registerPresenter(presenter, presenterView)

}
