package com.simfyafrica.ui.base.presenter

import android.content.Intent
import android.os.Bundle
import java.util.*


class PresenterCollection : IPresenterCallbacks, IPresenterCollection {
    private var isCreated: Boolean = false

    private val presenters = ArrayList<IPresenter<*>>()

    override fun onCreate(var1: Bundle?) {
        this.isCreated = true

        for (i in this.presenters.indices) {
            val presenter = this.presenters[i]
            var bundle: Bundle? = null
            if (var1 != null) {
                val key = this.getInstanceStateBundleKey(i)
                bundle = var1.getBundle(key)
            }

            presenter.onCreate(bundle)
        }
    }

    override fun onStart() {
        for (presenter in this.presenters) {
            presenter.onStart()
        }
    }

    override fun onResume() {
        for (presenter in this.presenters) {
            presenter.onResume()
        }
    }

    override fun onPause() {
        for (presenter in this.presenters) {
            presenter.onPause()
        }
    }

    override fun onStop() {
        for (presenter in this.presenters) {
            presenter.onStop()
        }
    }

    override fun onDestroy() {
        for (presenter in this.presenters) {
            presenter.onDestroy()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        for (i in this.presenters.indices) {
            val presenter = this.presenters[i]
            val key = this.getInstanceStateBundleKey(i)
            val bundle = Bundle()
            presenter.onSaveInstanceState(bundle)
            outState.putBundle(key, bundle)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
        val presenterIterator = this.presenters.iterator()

        var presenter: IPresenter<*>
        do {
            if (!presenterIterator.hasNext()) {
                return false
            }

            presenter = presenterIterator.next()
        } while (!presenter.onActivityResult(requestCode, resultCode, data))

        return true
    }

    override fun <TPresenter : IPresenter<TPresenterView>, TPresenterView> registerPresenter(
            presenter: TPresenter, presenterView: TPresenterView) = when {
        this.isCreated -> throw RuntimeException(
                "Cannot register presenter after onCreate")
        this.presenters.contains(presenter) -> throw RuntimeException(
                "This presenter already registered")
        else -> {
            this.presenters.add(presenter)
            presenter.setView(presenterView)
            presenter
        }
    }

    private fun getInstanceStateBundleKey(index: Int) = "presenter_" + index
}