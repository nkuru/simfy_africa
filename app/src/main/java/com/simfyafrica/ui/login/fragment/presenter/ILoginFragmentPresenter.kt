package com.simfyafrica.ui.login.fragment.presenter

import com.simfyafrica.ui.base.presenter.IPresenter
import com.simfyafrica.ui.login.fragment.view.ILoginFragmentView

interface ILoginFragmentPresenter : IPresenter<ILoginFragmentView> {

    fun validateInputs(username: String?, password: String?)
}