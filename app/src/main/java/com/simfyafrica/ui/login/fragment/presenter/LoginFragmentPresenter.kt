package com.simfyafrica.ui.login.fragment.presenter

import com.simfyafrica.pref.IPreferencesManager
import com.simfyafrica.ui.base.presenter.RxBasePresenter
import com.simfyafrica.ui.login.fragment.view.ILoginFragmentView


class LoginFragmentPresenter(
        private val preferencesManager: IPreferencesManager) :
        RxBasePresenter<ILoginFragmentView>(), ILoginFragmentPresenter {

    override fun validateInputs(username: String?, password: String?) {

        if(!validateUsername(username)){
            getView().onUsernameValidationError("")
            return
        }
        if(!validatePassword(password)){
            getView().onPasswordValidationError("")
            return
        }
        preferencesManager.setUsername(username)

        getView().onAllInputsValidated()

    }

    private fun validatePassword(password: String?): Boolean {
        return password != null && password.length >= 6
    }

    private fun validateUsername(username: String?): Boolean {
        return username != null && username.isNotEmpty()
    }


}