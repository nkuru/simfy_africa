package com.simfyafrica.ui.login.fragment.view

import com.simfyafrica.ui.base.IView

interface ILoginFragmentView : IView {

    fun onUsernameValidationError(error: String)

    fun onPasswordValidationError(error: String)

    fun onAllInputsValidated()

    fun showProgressDialog()

    fun hideProgressDialog()

}