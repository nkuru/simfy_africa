package com.simfyafrica.ui.login.activity.presenter

import com.simfyafrica.ui.base.presenter.IPresenter
import com.simfyafrica.ui.login.activity.view.ILoginActivityView

interface ILoginActivityPresenter :IPresenter<ILoginActivityView>

//todo -- Add interface body here