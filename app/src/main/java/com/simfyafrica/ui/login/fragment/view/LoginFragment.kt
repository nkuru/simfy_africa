package com.simfyafrica.ui.login.fragment.view

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.simfyafrica.R
import com.simfyafrica.inject.component.FragmentComponent
import com.simfyafrica.inject.module.fragment.LoginFragmentPresenterModule
import com.simfyafrica.ui.base.fragment.BaseComponentFragment
import com.simfyafrica.ui.login.fragment.presenter.ILoginFragmentPresenter
import com.simfyafrica.ui.placehold.activity.view.PlaceholdActivity
import kotlinx.android.synthetic.main.fragment_login.*
import javax.inject.Inject


class LoginFragment : BaseComponentFragment(), ILoginFragmentView {


    @Inject
    lateinit var loginFragmentPresenter: ILoginFragmentPresenter


    companion object {
        fun newInstance(): LoginFragment {
            return LoginFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(
            R.layout.fragment_login, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpListeners()
    }

    private fun setUpListeners() {
        btnLogin.setOnClickListener {
            if (!isConnectedOrConnecting(getViewContext())) {
                showDialog(getString(R.string.please_connect_to_internet))

            } else {
                loginFragmentPresenter.validateInputs(etUsername.text.toString(), etPassword.text.toString())
            }
        }
    }


    override fun inject(fragmentComponent: FragmentComponent) {
        fragmentComponent.plus(LoginFragmentPresenterModule()).inject(this)
        registerPresenter(loginFragmentPresenter, this)
    }

    override fun logAction(name: String) {
        //todo -- log action
    }

    override fun onUsernameValidationError(error: String) {
        etUsername.error = getString(R.string.username_cannot_be_empty)
        etUsername.requestFocus()
    }

    override fun onPasswordValidationError(error: String) {
        etPassword.error = getString(R.string.password_error)
        etPassword.requestFocus()
    }

    override fun onAllInputsValidated() {
        showProgress()

        //FIXME -- Usually we'd call an end point to login but I will just show a fake progress bar for 3 seconds
        val handler = Handler()
        handler.postDelayed({
            startActivity(Intent(activity, PlaceholdActivity::class.java))
            hideProgressDialog()
        }, 3000)

    }

    override fun showProgressDialog() {
        //todo

    }


    override fun hideProgressDialog() {
        hideProgress()
    }

}