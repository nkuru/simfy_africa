package com.simfyafrica.ui.login.activity.view

import android.os.Bundle
import com.simfyafrica.R
import com.simfyafrica.inject.component.ActivityComponent
import com.simfyafrica.inject.module.activity.LoginActivityPresenterModule
import com.simfyafrica.pref.IPreferencesManager
import com.simfyafrica.ui.base.activity.component.BaseComponentActivity
import com.simfyafrica.ui.login.activity.presenter.ILoginActivityPresenter
import com.simfyafrica.ui.login.fragment.view.LoginFragment
import javax.inject.Inject

class LoginActivity : BaseComponentActivity(),
        ILoginActivityView {


    @Inject
    lateinit var preferencesManager: IPreferencesManager

    @Inject
    lateinit var loginPresenter: ILoginActivityPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        afterViews()
    }

    private fun afterViews() {
        switchFragment(LoginFragment.newInstance(), true, false, true)
    }

    override fun inject(activityComponent: ActivityComponent) {
        activityComponent.plus(LoginActivityPresenterModule()).inject(this)
        registerPresenter(loginPresenter, this)
    }

    override fun logAction(name: String) {
        //todo --
    }

    /*override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) {
            hideSystemUI()
        }
    }*/

    override fun showDialog(message: String) {
        //todo
    }
}
