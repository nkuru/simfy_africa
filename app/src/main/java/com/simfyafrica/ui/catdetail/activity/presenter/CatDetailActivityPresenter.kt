package com.simfyafrica.ui.catdetail.activity.presenter

import com.simfyafrica.ui.base.presenter.RxBasePresenter
import com.simfyafrica.ui.catdetail.activity.view.ICatDetailActivityView

class CatDetailActivityPresenter : RxBasePresenter<ICatDetailActivityView>(), ICatDetailActivityPresenter

//todo