package com.simfyafrica.ui.catdetail.activity.presenter

import com.simfyafrica.ui.base.presenter.IPresenter
import com.simfyafrica.ui.catdetail.activity.view.ICatDetailActivityView

interface ICatDetailActivityPresenter : IPresenter<ICatDetailActivityView>