package com.simfyafrica.ui.catdetail.fragment.presenter

import com.simfyafrica.ui.base.presenter.IPresenter
import com.simfyafrica.ui.catdetail.fragment.view.ICatDetailFragmentView

interface ICatDetailFragmentPresenter :IPresenter<ICatDetailFragmentView>