package com.simfyafrica.ui.catdetail.fragment.view

import android.net.Uri
import android.os.Build.VERSION.SDK_INT
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.google.android.exoplayer2.DefaultLoadControl
import com.google.android.exoplayer2.DefaultRenderersFactory
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.simfyafrica.R
import com.simfyafrica.inject.component.FragmentComponent
import com.simfyafrica.inject.module.fragment.CatDetailFragmentPresenterModule
import com.simfyafrica.ui.base.fragment.BaseComponentFragment
import com.simfyafrica.ui.catdetail.fragment.presenter.ICatDetailFragmentPresenter
import kotlinx.android.synthetic.main.fragment_cat_detail.*
import javax.inject.Inject


class CatDetailFragment : BaseComponentFragment(), ICatDetailFragmentView {


    @Inject
    lateinit var presenter: ICatDetailFragmentPresenter

    companion object {
        const val URL = "URL"
        const val TITLE = "TITLE"
        const val DESCRIPTION = "DESCRIPTION"

        fun newInstance(): CatDetailFragment {
            return CatDetailFragment()
        }
    }

    private lateinit var player: SimpleExoPlayer

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(
            R.layout.fragment_cat_detail, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        afterViews()
    }


    private fun afterViews() {

        tvDescription.text = arguments?.getString(DESCRIPTION)
        Glide.with(ivHeader)
                .load(arguments?.getString(URL))
                .into(ivHeader)

        fabPlay.setOnClickListener {
            if (player.playWhenReady) {
                player.playWhenReady=false
                player.playWhenReady=true
            } else player.playWhenReady = true
        }

    }

    private fun initializePlayer() {
        player = ExoPlayerFactory.newSimpleInstance(
                DefaultRenderersFactory(getViewContext()),
                DefaultTrackSelector(), DefaultLoadControl())

        playerView.player = player


        val mediaSource = buildMediaSource()
        player.prepare(mediaSource, true, false)
        player.playWhenReady = false
    }

    private fun buildMediaSource(): MediaSource {
        return ExtractorMediaSource(
                Uri.parse("asset:///cat_meow.wav"),
                DefaultDataSourceFactory(context, "simfy-africa"),
                DefaultExtractorsFactory(),
                null, null)
    }

    override fun onStart() {
        super.onStart()
        if (SDK_INT > 23) {
            initializePlayer()
        }
    }

    override fun onResume() {
        super.onResume()
        if (SDK_INT <= 23) {
            initializePlayer()
        }
    }


    override fun inject(fragmentComponent: FragmentComponent) {
        fragmentComponent.plus(CatDetailFragmentPresenterModule()).inject(this)
        registerPresenter(presenter, this)
    }

    override fun logAction(name: String) {
        //todo -- log action
    }


}