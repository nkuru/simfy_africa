package com.simfyafrica.ui.catdetail.activity.view

import android.os.Bundle
import android.view.MenuItem
import com.simfyafrica.R
import com.simfyafrica.inject.component.ActivityComponent
import com.simfyafrica.inject.module.activity.CatDetailActivityPresenterModule
import com.simfyafrica.ui.base.activity.component.BaseComponentActivity
import com.simfyafrica.ui.catdetail.activity.presenter.ICatDetailActivityPresenter
import com.simfyafrica.ui.catdetail.fragment.view.CatDetailFragment
import javax.inject.Inject

class CatDetailActivity : BaseComponentActivity(),
        ICatDetailActivityView {

    @Inject
    lateinit var presenter: ICatDetailActivityPresenter


    override fun inject(activityComponent: ActivityComponent) {
        activityComponent.plus(CatDetailActivityPresenterModule()).inject(this)
        registerPresenter(presenter, this)
    }

    override fun logAction(name: String) {
        //todo
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cat_detail)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        afterViews()
    }


    private fun afterViews() {
        title = intent.getStringExtra(CatDetailFragment.TITLE)
        val fragment = CatDetailFragment.newInstance()
        fragment.arguments = intent.extras
        switchFragment(fragment, true, false, true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showDialog(message: String) {
        //todo
    }
}
