package com.simfyafrica.ui.main.activity.presenter

import com.simfyafrica.ui.base.presenter.IPresenter
import com.simfyafrica.ui.main.activity.view.IMainActivityView

interface IMainActivityPresenter : IPresenter<IMainActivityView> {

    //FIXME -- This activity presenter interface does not do much but we'll keep it here nonetheless
}