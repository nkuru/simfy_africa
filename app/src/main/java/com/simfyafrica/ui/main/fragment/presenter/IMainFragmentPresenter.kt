package com.simfyafrica.ui.main.fragment.presenter

import com.simfyafrica.ui.base.presenter.IPresenter
import com.simfyafrica.ui.main.fragment.view.IMainFragmentView

interface IMainFragmentPresenter : IPresenter<IMainFragmentView> {

    fun goToTask1Clicked()

    fun goToTask2Clicked()

    fun goToTask3Clicked()

    fun clearData()
}