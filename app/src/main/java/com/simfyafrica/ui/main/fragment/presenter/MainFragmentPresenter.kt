package com.simfyafrica.ui.main.fragment.presenter

import com.simfyafrica.database.AppDatabase
import com.simfyafrica.pref.IPreferencesManager
import com.simfyafrica.ui.base.presenter.RxBasePresenter
import com.simfyafrica.ui.main.fragment.view.IMainFragmentView


class MainFragmentPresenter(
        private val preferencesManager: IPreferencesManager, private val appDatabase: AppDatabase) :
        RxBasePresenter<IMainFragmentView>(), IMainFragmentPresenter {


    override fun clearData() {
        appDatabase.imageDao().deleteAll()
        preferencesManager.clearData()
        com.simfyafrica.utils.FileUtils.resetAllFiles()
        //todo -- Clear database
    }

    override fun goToTask1Clicked() {
        if (preferencesManager.getUsername.isNotEmpty()) {
            getView().goToItemListActivity()
        } else {
            getView().goToLoginActivity()
        }
    }

    override fun goToTask2Clicked() {
        getView().onGoToTask2()
    }

    override fun goToTask3Clicked() {
        getView().onGoToTask3()
    }

}