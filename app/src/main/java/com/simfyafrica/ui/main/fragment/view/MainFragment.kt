package com.simfyafrica.ui.main.fragment.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.simfyafrica.R
import com.simfyafrica.inject.component.FragmentComponent
import com.simfyafrica.inject.module.fragment.MainFragmentPresenterModule
import com.simfyafrica.ui.base.fragment.BaseComponentFragment
import com.simfyafrica.ui.catapi.activity.view.CatListActivity
import com.simfyafrica.ui.login.activity.view.LoginActivity
import com.simfyafrica.ui.main.fragment.presenter.IMainFragmentPresenter
import com.simfyafrica.ui.placehold.activity.view.PlaceholdActivity
import com.simfyafrica.ui.task3.Task3Activity
import kotlinx.android.synthetic.main.fragment_main.*
import javax.inject.Inject

class MainFragment : BaseComponentFragment(), IMainFragmentView {


    @Inject
    lateinit var mainFragmentPresenter: IMainFragmentPresenter


    companion object {
        fun newInstance(): MainFragment {
            return MainFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(
            R.layout.fragment_main, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpListeners()
    }

    private fun setUpListeners() {
        btnTask1.setOnClickListener { mainFragmentPresenter.goToTask1Clicked() }
        btnTask2.setOnClickListener { mainFragmentPresenter.goToTask2Clicked() }
        btnTask3.setOnClickListener { mainFragmentPresenter.goToTask3Clicked() }
    }


    override fun inject(fragmentComponent: FragmentComponent) {
        fragmentComponent.plus(MainFragmentPresenterModule()).inject(this)
        registerPresenter(mainFragmentPresenter, this)
    }

    override fun logAction(name: String) {
        //todo -- log action
    }

    override fun goToItemListActivity() {
        startActivity(Intent(activity, PlaceholdActivity::class.java))
    }

    override fun goToLoginActivity() {
        startActivity(Intent(activity, LoginActivity::class.java))
    }

    override fun onGoToTask2() {
        startActivity(Intent(activity, CatListActivity::class.java))
    }

    override fun onGoToTask3() {
        startActivity(Intent(activity, Task3Activity::class.java))
    }
}