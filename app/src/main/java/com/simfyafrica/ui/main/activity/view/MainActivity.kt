package com.simfyafrica.ui.main.activity.view

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.util.Log
import com.simfyafrica.R
import com.simfyafrica.app.SimfyAfricaApp
import com.simfyafrica.inject.component.ActivityComponent
import com.simfyafrica.inject.module.activity.MainActivityPresenterModule
import com.simfyafrica.pref.IPreferencesManager
import com.simfyafrica.ui.base.activity.component.BaseComponentActivity
import com.simfyafrica.ui.main.activity.presenter.IMainActivityPresenter
import com.simfyafrica.ui.main.fragment.view.MainFragment
import javax.inject.Inject

open class MainActivity : BaseComponentActivity(),
        IMainActivityView {



    @Inject
    lateinit var preferencesManager: IPreferencesManager

    @Inject
    lateinit var mainActivityPresenter: IMainActivityPresenter


    override fun inject(activityComponent: ActivityComponent) {
        activityComponent.plus(MainActivityPresenterModule()).inject(this)
        registerPresenter(mainActivityPresenter, this)
    }

    override fun logAction(name: String) {
        //todo
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        afterViews()
        Log.e("StoragePath", ContextCompat.getExternalFilesDirs(SimfyAfricaApp.application, null)[0].absolutePath)
    }

    private fun afterViews() {
        switchFragment(MainFragment.newInstance(), true, false, true)
    }

    /*override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) {
            hideSystemUI()
        }
    }*/

    override fun showDialog(message: String) {
        //todo
    }


}
