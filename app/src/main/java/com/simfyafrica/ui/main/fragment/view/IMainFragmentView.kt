package com.simfyafrica.ui.main.fragment.view

import com.simfyafrica.ui.base.IView

interface IMainFragmentView : IView{

    fun goToLoginActivity()

    fun goToItemListActivity()

    fun onGoToTask2()

    fun onGoToTask3()

}