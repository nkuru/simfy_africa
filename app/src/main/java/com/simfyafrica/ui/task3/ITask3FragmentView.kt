package com.simfyafrica.ui.task3

import com.simfyafrica.ui.base.IView

interface ITask3FragmentView : IView {

    fun showInputError()

    fun showWritePermissionRequestDialog()

    fun showDecrypt()

    fun showEncrypt()

    fun showMoveToInternal()

    fun showMoveToExternal()
}