package com.simfyafrica.ui.task3

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.simfyafrica.R
import com.simfyafrica.inject.component.FragmentComponent
import com.simfyafrica.inject.module.fragment.Task3FragmentPresenterModule
import com.simfyafrica.ui.base.fragment.BaseComponentFragment
import kotlinx.android.synthetic.main.fragment_task3.*
import javax.inject.Inject

class Task3Fragment : BaseComponentFragment(), ITask3FragmentView {


    @Inject
    lateinit var presenter: ITask3FragmentPresenter


    companion object {
        fun newInstance(): Task3Fragment {
            return Task3Fragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(
            R.layout.fragment_task3, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        afterViews()
    }


    private fun afterViews() {
        addListeners()
    }

    private fun addListeners() {
        ivCreate.setOnClickListener { presenter.create(etInput.text.toString()) }
        ivDelete.setOnClickListener { presenter.delete(etInput.text.toString()) }
        ivMove.setOnClickListener { presenter.move(etInput.text.toString()) }
        ivEncrypt.setOnClickListener { presenter.encryptOrDecrypt(etInput.text.toString(), getString(R.string.ENCRYPTION_KEY)) }
        ivReset.setOnClickListener { presenter.reset() }

        etInput.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!p0.toString().isEmpty() && p0.toString().toInt() in 1..100) {
                    presenter.checkFileExists(p0.toString())
                } else {
                    etInput.error = getString(R.string.range_error)
                }
            }
        })
    }


    override fun inject(fragmentComponent: FragmentComponent) {
        fragmentComponent.plus(Task3FragmentPresenterModule()).inject(this)
        registerPresenter(presenter, this)
    }

    override fun logAction(name: String) {
        showProgress()
        //todo -- log action
    }

    override fun showInputError() {

    }

    override fun showWritePermissionRequestDialog() {

    }

    override fun showDecrypt() {
        ivEncrypt.text = getString(R.string.decrypt)
    }

    override fun showEncrypt() {
        ivEncrypt.text = getString(R.string.encrypt)
    }

    override fun showMoveToInternal() {
        ivMove.text = getString(R.string.move_to_internal)
    }

    override fun showMoveToExternal() {
        ivMove.text = getString(R.string.move_to_external)
    }

}