package com.simfyafrica.ui.task3

import android.Manifest
import android.util.Log
import com.google.gson.Gson
import com.simfyafrica.api.RetrofitService
import com.simfyafrica.database.AppDatabase
import com.simfyafrica.ui.base.presenter.RxBasePresenter
import com.simfyafrica.utils.FileUtils
import com.tbruyelle.rxpermissions.RxPermissions
import retrofit2.Retrofit
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class Task3FragmentPresenter(private val appDatabase: AppDatabase,
                             var retrofit: Retrofit, var gson: Gson) : RxBasePresenter<ITask3FragmentView>(), ITask3FragmentPresenter {


    override fun create(number: String) {
        if (!validateInput(number)) {
            getView().showInputError()
            return
        }

        if (FileUtils.fileExists(number)) {
            getView().showDialog("File already exists")
            return
        }

        if (!checkWritePermissionGranted()) {
            RxPermissions.getInstance(getView().getViewContext())
                    .request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .subscribe({ granted ->
                        if (granted!!) {
                            createFile(number)
                        }
                    }, { getView().showDialog("We need storage permission") })
        } else {
            createFile(number)
        }

    }

    private fun createFile(number: String) {
        addSubscriber(retrofit.create(RetrofitService::class.java).getPokeResponse(number)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(applyProgress())
                .subscribe({
                    if (FileUtils.createFile(number, gson.toJson(it)) == null) {
                        it.storageLocation = 2
                        it.security = 2
                        appDatabase.pokeDao().insert(it)
                        getView().showMoveToInternal()
                        getView().showEncrypt()
                        Log.e("All DB values", gson.toJson(appDatabase.pokeDao().getAll().size))
                        getView().showDialog("File saved successfully")
                    } else {
                        getView().showDialog("Some error happened! Sorry")
                    }
                }, { it.message?.let { it1 -> getView().showDialog(it1) } }))
    }

    private fun moveFile(number: String) {
        if (!FileUtils.fileExists(number)) {
            getView().showDialog("File does not exist")
            return
        }
        if (FileUtils.moveFile(number) == null) {
            val pokeResponse = appDatabase.pokeDao().getResponse(number.toLong())
            if (pokeResponse[0].storageLocation == 2) {
                pokeResponse[0].storageLocation = 1
                appDatabase.pokeDao().insert(pokeResponse[0])
                getView().showMoveToExternal()
            } else {
                pokeResponse[0].storageLocation = 2
                appDatabase.pokeDao().insert(pokeResponse[0])
                getView().showMoveToInternal()
            }
            getView().showDialog("File moved successfully")
        } else getView().showDialog("Some error happened! Sorry")
    }


    override fun move(number: String) {
        if (!validateInput(number)) {
            getView().showInputError()
            return
        }
        if (!checkWritePermissionGranted()) {
            RxPermissions.getInstance(getView().getViewContext())
                    .request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .subscribe({ granted ->
                        if (granted!!) {
                            moveFile(number)
                        }
                    }, { getView().showDialog("We need storage permission") })
        } else {
            moveFile(number)
        }

    }

    override fun checkFileExists(number: String) {
        if (FileUtils.fileExists(number)) {
            if (appDatabase.pokeDao().getLocation(number.toLong()) == 2) {
                getView().showMoveToInternal()
            } else {
                getView().showMoveToExternal()
            }
            if (appDatabase.pokeDao().getSecurityType(number.toLong()) == 2) {
                getView().showEncrypt()
            } else {
                getView().showDecrypt()
            }
        }
    }

    private fun deleteFile(number: String) {
        if (FileUtils.deleteFile(number) == null) {
            appDatabase.pokeDao().deleteElement(number.toLong())
            getView().showDialog("File deleted successfully")
        } else getView().showDialog("Some error happened! Sorry")
    }

    override fun delete(number: String) {
        if (!FileUtils.fileExists(number)) {
            getView().showDialog("File does not exist")
            return
        }
        if (!validateInput(number)) {
            getView().showInputError()
            return
        }


        if (!checkWritePermissionGranted()) {
            RxPermissions.getInstance(getView().getViewContext())
                    .request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .subscribe({ granted ->
                        if (granted!!) {
                            deleteFile(number)
                        }
                    }, { getView().showDialog("We need storage permission") })
        } else {
            deleteFile(number)
        }


    }

    private fun encryptFile(number: String, encryptionKey: String) {
        val pokeResponse = appDatabase.pokeDao().getResponse(number.toLong())
        if (pokeResponse[0].security == 2) {
            if(FileUtils.encryptFile(number, encryptionKey)==null){
                pokeResponse[0].security=1
                appDatabase.pokeDao().insert(pokeResponse[0])
                getView().showDialog("File encrypted successfully")
                getView().showDecrypt()
            }else getView().showDialog("Some error happened! Sorry")
        }else{
            if(FileUtils.decryptFile(number, encryptionKey)==null){
                pokeResponse[0].security=2
                appDatabase.pokeDao().insert(pokeResponse[0])
                getView().showDialog("File decrypted successfully")
                getView().showEncrypt()
            }else getView().showDialog("Some error happened! Sorry")
        }
    }

    override fun encryptOrDecrypt(number: String, encryptionKey:String) {
        if (!validateInput(number)) {
            getView().showInputError()
            return
        }

        if (!FileUtils.fileExists(number)) {
            getView().showDialog("File does not exist")
            return
        }

        if (!checkWritePermissionGranted()) {
            RxPermissions.getInstance(getView().getViewContext())
                    .request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .subscribe({ granted ->
                        if (granted!!) {
                            encryptFile(number, encryptionKey)
                        }
                    }, { getView().showDialog("We need storage permission") })
        } else {
            encryptFile(number, encryptionKey)
        }
    }

    private fun resetAllFiles() {
        appDatabase.pokeDao().deleteAll()
        FileUtils.resetAllFiles()
        getView().showDialog("All data deleted in the DB and file systems")
    }

    override fun reset() {
        if (!checkWritePermissionGranted()) {
            RxPermissions.getInstance(getView().getViewContext())
                    .request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .subscribe({ granted ->
                        if (granted!!) {
                            appDatabase.pokeDao().deleteAll()
                            resetAllFiles()
                        }
                    }, { getView().showDialog("We need storage permission") })
        } else {
            appDatabase.pokeDao().deleteAll()
            resetAllFiles()
        }
    }


    private fun validateInput(number: String): Boolean {
        return !number.isEmpty()
    }

    private fun checkWritePermissionGranted(): Boolean {
        val permissionStatus = RxBasePresenter.Companion.getPermissionStatus(
                getView().getViewContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return permissionStatus == RxBasePresenter.Companion.GRANTED

    }


}