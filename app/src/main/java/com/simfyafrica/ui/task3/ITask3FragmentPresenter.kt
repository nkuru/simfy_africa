package com.simfyafrica.ui.task3

import com.simfyafrica.ui.base.presenter.IPresenter

interface ITask3FragmentPresenter :IPresenter<ITask3FragmentView>{

    fun create(number: String)

    fun move(number: String)

    fun delete(number: String)

    fun encryptOrDecrypt(number: String, encryptionKey:String)

    fun reset()

    fun checkFileExists(number: String)
}