package com.simfyafrica.ui.task3

import android.os.Bundle
import android.view.MenuItem
import com.simfyafrica.R
import com.simfyafrica.inject.component.ActivityComponent
import com.simfyafrica.inject.module.activity.Task3ActivityPresenterModule
import com.simfyafrica.ui.base.activity.component.BaseComponentActivity
import javax.inject.Inject

class Task3Activity : BaseComponentActivity(),
        ITask3ActivityView {


    @Inject
    lateinit var presenter: ITask3ActivityPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task3)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        afterViews()
    }

    private fun afterViews() {
        switchFragment(Task3Fragment.newInstance(), true, false, true)
    }


    override fun inject(activityComponent: ActivityComponent) {
        activityComponent.plus(Task3ActivityPresenterModule()).inject(this)
        registerPresenter(presenter, this)
    }

    override fun logAction(name: String) {

    }
    override fun showDialog(message: String) {
        //todo
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
