package com.simfyafrica.ui.catapi.fragment.presenter

import com.simfyafrica.ui.base.presenter.IPresenter
import com.simfyafrica.ui.catapi.fragment.view.ICatListFragmentView

interface ICatListFragmentPresenter : IPresenter<ICatListFragmentView> {

    fun getData()
}