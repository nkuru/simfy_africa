package com.simfyafrica.ui.catapi.activity.presenter

import com.simfyafrica.ui.base.presenter.IPresenter
import com.simfyafrica.ui.catapi.activity.view.ICatListAtivityView

interface ICatListAtivityPresenter: IPresenter<ICatListAtivityView>

//todo