package com.simfyafrica.ui.catapi.activity.view

import android.os.Bundle
import android.view.MenuItem
import com.simfyafrica.R
import com.simfyafrica.inject.component.ActivityComponent
import com.simfyafrica.inject.module.activity.CatListActivityPresenterModule
import com.simfyafrica.ui.base.activity.component.BaseComponentActivity
import com.simfyafrica.ui.catapi.activity.presenter.ICatListAtivityPresenter
import com.simfyafrica.ui.catapi.fragment.view.CatListFragment
import javax.inject.Inject

class CatListActivity : BaseComponentActivity(),
        ICatListAtivityView {



    @Inject
    lateinit var presenter: ICatListAtivityPresenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cat_list)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        afterViews()
    }

    private fun afterViews() {
        switchFragment(CatListFragment.newInstance(), true, false, true)
    }


    override fun inject(activityComponent: ActivityComponent) {
        activityComponent.plus(CatListActivityPresenterModule()).inject(this)
        registerPresenter(presenter, this)
    }

    override fun logAction(name: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showDialog(message: String) {
        //todo
    }


}
