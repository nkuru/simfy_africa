package com.simfyafrica.ui.catapi.fragment.presenter

import com.simfyafrica.api.RetrofitService
import com.simfyafrica.database.AppDatabase
import com.simfyafrica.models.ImageItem
import com.simfyafrica.ui.base.presenter.RxBasePresenter
import com.simfyafrica.ui.catapi.fragment.view.ICatListFragmentView
import retrofit2.Retrofit
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class CatListFragmentPresenter(private val appDatabase: AppDatabase,
                               var retrofit: Retrofit) :
        RxBasePresenter<ICatListFragmentView>(), ICatListFragmentPresenter {

    override fun getData() {
        val images = appDatabase.imageDao().getImages()
        handleDatabaseResponse(images)
    }

    private fun handleDatabaseResponse(images: MutableList<ImageItem>) {
        if (images.isEmpty() || images.size < 1) requestDataFromNet() else handleData(images)
    }

    private fun handleData(images: MutableList<ImageItem>) {
        getView().showImages(images)
    }

    private fun requestDataFromNet() {
        addSubscriber(retrofit.create(RetrofitService::class.java).getImages()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(applyProgress())
                .subscribe({
                    val images = fillFields(it.data.images)
                    appDatabase.imageDao().insert(images) //FIXME -- I am not checking if the insertion was successful or not
                    handleData(images)
                }, { this.handleError(it) }))
    }

    private fun fillFields(images: MutableList<ImageItem>): MutableList<ImageItem> {
        for (i in images.indices) {
            images[i].title = "Image ${i + 1}"
            images[i].description = "This is the description for ${images[i].title}, It's a really cool image,\n" +
                    "bask in it's glorious"
        }
        return images
    }


}