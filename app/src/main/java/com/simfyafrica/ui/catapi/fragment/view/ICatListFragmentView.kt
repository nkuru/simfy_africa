package com.simfyafrica.ui.catapi.fragment.view

import com.simfyafrica.models.ImageItem
import com.simfyafrica.ui.base.IView

interface ICatListFragmentView: IView{

    fun showImages(images: List<ImageItem>)
}