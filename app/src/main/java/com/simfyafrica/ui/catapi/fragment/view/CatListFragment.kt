package com.simfyafrica.ui.catapi.fragment.view

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.simfyafrica.R
import com.simfyafrica.inject.component.FragmentComponent
import com.simfyafrica.inject.module.fragment.CatListFragmentPresenterModule
import com.simfyafrica.models.ImageItem
import com.simfyafrica.ui.base.adapter.OnItemClickListener
import com.simfyafrica.ui.base.fragment.BaseComponentFragment
import com.simfyafrica.ui.catapi.CatListAdapter
import com.simfyafrica.ui.catapi.fragment.presenter.ICatListFragmentPresenter
import com.simfyafrica.ui.catdetail.activity.view.CatDetailActivity
import com.simfyafrica.ui.catdetail.fragment.view.CatDetailFragment
import kotlinx.android.synthetic.main.fragment_placehold.*
import javax.inject.Inject

class CatListFragment : BaseComponentFragment(), ICatListFragmentView {


    private var mAdapter: CatListAdapter? = null
    private var imageList = mutableListOf<ImageItem>()

    private var mLayoutManager: RecyclerView.LayoutManager? = null

    @Inject
    lateinit var presenter: ICatListFragmentPresenter


    companion object {
        fun newInstance(): CatListFragment {
            return CatListFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(
            R.layout.fragment_cat_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        afterViews()
    }


    private fun afterViews() {
        initializeRecyclerView()
        if (!isConnectedOrConnecting(getViewContext())) {
            showDialog(
                    getString(R.string.please_connect_to_internet))
        } else {
            presenter.getData()
        }
    }

    private fun initializeRecyclerView() {
        mAdapter = CatListAdapter(imageList, activity)
        val onItemClickListener: OnItemClickListener = object : OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                val activity = Intent(activity, CatDetailActivity::class.java)
                activity.putExtra(CatDetailFragment.URL, imageList[position].url)
                activity.putExtra(CatDetailFragment.TITLE, imageList[position].title)
                activity.putExtra(CatDetailFragment.DESCRIPTION, imageList[position].description)
                startActivity(activity)
            }

        }
        mAdapter?.setListener(onItemClickListener)
        mLayoutManager = LinearLayoutManager(activity)
        recyclerView.layoutManager = mLayoutManager
        val dividerItemDecoration = DividerItemDecoration(recyclerView.context,
                LinearLayoutManager.VERTICAL)
        recyclerView.addItemDecoration(dividerItemDecoration)
        recyclerView.adapter = mAdapter

    }

    override fun showImages(images: List<ImageItem>) {
        imageList.addAll(images)
        mAdapter?.notifyDataSetChanged()
    }


    override fun inject(fragmentComponent: FragmentComponent) {
        fragmentComponent.plus(CatListFragmentPresenterModule()).inject(this)
        registerPresenter(presenter, this)
    }

    override fun logAction(name: String) {
        //todo -- log action
    }

}