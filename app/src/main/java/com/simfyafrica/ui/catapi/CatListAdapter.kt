package com.simfyafrica.ui.catapi

import android.content.Context
import android.graphics.BitmapFactory
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.simfyafrica.R
import com.simfyafrica.inject.module.GlideApp
import com.simfyafrica.models.ImageItem
import com.simfyafrica.ui.base.adapter.BaseAdapter
import com.simfyafrica.ui.base.adapter.OnItemClickListener
import kotlinx.android.synthetic.main.cat_list_item.view.*

class CatListAdapter(data: MutableList<ImageItem>, context: Context?) : BaseAdapter<ImageItem, BaseAdapter.ViewHolder>(data) {

    var mListener: OnItemClickListener? = null
    var mContext: Context? = null

    init {
        mContext = context
    }

    fun setListener(listener: OnItemClickListener) {
        this.mListener = listener
    }

    override fun onBindViewHolder(holder: BaseAdapter.ViewHolder, position: Int) {
        mListener?.let { (holder as DataViewHolder).bind(mData[position], mContext) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder {
        val holder = LayoutInflater.from(parent.context)
                .inflate(R.layout.cat_list_item, parent, false) as View
        return mListener?.let { DataViewHolder(holder, it) }!!
    }

    class DataViewHolder(holder: View, listener: OnItemClickListener) : BaseAdapter.ViewHolder(holder, listener) {

        fun bind(item: ImageItem, context: Context?) {
            itemView.tvTitle.text = item.title
            itemView.tvDescription.text = item.description
            context?.let {
                itemView.ivImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.placeholder))
            }
            GlideApp.with(this.itemView)
                    .load(item.url)
                    .placeholder(R.drawable.placeholder)
                    .into(itemView.ivImage)
        }
    }
}
