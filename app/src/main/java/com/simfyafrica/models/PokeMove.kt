package com.simfyafrica.models

import com.google.gson.annotations.SerializedName

data class PokeMove(

        @SerializedName("move")
        var move: Move
)