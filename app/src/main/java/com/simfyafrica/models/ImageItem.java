package com.simfyafrica.models;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;


@Entity(tableName = "images")
@Root(name = "image", strict = false)
public class ImageItem {

    @PrimaryKey(autoGenerate = true)
    @Element(required = false)
    private int uid;

    @ColumnInfo(name = "title")
    @Element(required = false)
    private String title;

    @ColumnInfo(name = "url")
    @Element(name = "url")
    private String url;

    @ColumnInfo(name = "description")
    @Element(required = false)
    private String description;


    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
