package com.simfyafrica.models

import com.google.gson.annotations.SerializedName

data class PokeType(

        @SerializedName("slot")
        var slot: Int,

        @SerializedName("type")
        var type: Type

        )