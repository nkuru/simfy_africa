package com.simfyafrica.models

import com.google.gson.annotations.SerializedName


data class PokeAbility(

        @SerializedName("slot")
        var slot: Int,

        @SerializedName("is_hidden")
        var hidden: Boolean,

        @SerializedName("ability")
        var ability: Ability

)