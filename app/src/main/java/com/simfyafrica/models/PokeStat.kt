package com.simfyafrica.models

import android.arch.persistence.room.ColumnInfo
import com.google.gson.annotations.SerializedName

data class PokeStat(

        @SerializedName("base_stat")
        var baseStat: Int,

        @SerializedName("effort")
        var effort: Int,

        @SerializedName("stat")
        var stat: Stat,

        @ColumnInfo(name = "poke_stat_id")
        var pokeStatId: Long

)