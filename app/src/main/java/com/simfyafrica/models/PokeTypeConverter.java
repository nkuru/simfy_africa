package com.simfyafrica.models;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.Collections;
import java.util.List;

public class PokeTypeConverter {

    private Gson gson = new Gson();

    @TypeConverter
    public List<PokeType> toList(String data) {
        if (data.isEmpty()) return Collections.emptyList();
        return gson.fromJson(data, new TypeToken<List<PokeType>>() {
        }.getType());
    }

    @TypeConverter
    public String fromList(List<PokeType> data) {
        return gson.toJson(data);
    }
}
