package com.simfyafrica.models;

import org.simpleframework.xml.Element;


public class Response {
    @Element(name = "data")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
