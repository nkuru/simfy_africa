package com.simfyafrica.models;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.Collections;
import java.util.List;

public class PokeAbilityConverter {

    private Gson gson = new Gson();

    @TypeConverter
    public List<PokeAbility> toList(String data) {
        if (data.isEmpty()) return Collections.emptyList();
        return gson.fromJson(data, new TypeToken<List<PokeAbility>>() {
        }.getType());
    }

    @TypeConverter
    public String fromList(List<PokeAbility> data) {
        return gson.toJson(data);
    }
}
