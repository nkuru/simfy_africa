package com.simfyafrica.models;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;

import java.util.List;

public class Data {
    @ElementList(name = "image", inline = true)
    @Path("images")
    private List<ImageItem> images;

    public List<ImageItem> getImages() {
        return images;
    }

    public void setImages(List<ImageItem> images) {
        this.images = images;
    }
}
