package com.simfyafrica.models

import com.google.gson.annotations.SerializedName

data class Type(

        @SerializedName("url")
        var url: String,

        @SerializedName("name")
        var name: String
)