package com.simfyafrica.models

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class Move (

        @SerializedName("url")
        var url:String,

        @SerializedName("name")
        var name:String,

        @ColumnInfo(name = "move_id")
        var typeId: Long

)