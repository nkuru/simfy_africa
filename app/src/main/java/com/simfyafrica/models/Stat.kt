package com.simfyafrica.models

import com.google.gson.annotations.SerializedName

data class Stat(

        @SerializedName("url")
        var url: String,

        @SerializedName("name")
        var name: String
)