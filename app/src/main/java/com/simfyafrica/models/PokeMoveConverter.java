package com.simfyafrica.models;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.Collections;
import java.util.List;

public class PokeMoveConverter {

    private Gson gson = new Gson();

    @TypeConverter
    public List<PokeMove> toList(String data) {
        if (data.isEmpty()) return Collections.emptyList();
        return gson.fromJson(data, new TypeToken<List<PokeMove>>() {
        }.getType());
    }

    @TypeConverter
    public String fromList(List<PokeMove> data) {
        return gson.toJson(data);
    }
}
