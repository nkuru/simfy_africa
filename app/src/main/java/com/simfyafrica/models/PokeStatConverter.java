package com.simfyafrica.models;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.Collections;
import java.util.List;

public class PokeStatConverter {
    private Gson gson = new Gson();

    @TypeConverter
    public List<PokeStat> toList(String data) {
        if (data.isEmpty()) return Collections.emptyList();
        return gson.fromJson(data, new TypeToken<List<PokeStat>>() {
        }.getType());
    }

    @TypeConverter
    public String fromList(List<PokeStat> data) {
        return gson.toJson(data);
    }
}
