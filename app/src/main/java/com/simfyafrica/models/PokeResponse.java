package com.simfyafrica.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverter;
import android.arch.persistence.room.TypeConverters;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity(tableName = "poke_data")
public class PokeResponse {

    @PrimaryKey
    private long id;

    @SerializedName("height")
    private String height;

    @SerializedName("weight")
    private String weight;

    @SerializedName("stats")
    @TypeConverters(PokeStatConverter.class)
    private List<PokeStat> stats=null;

    @SerializedName("abilities")
    @TypeConverters(PokeAbilityConverter.class)
    private  List<PokeAbility>abilities;

    @SerializedName("types")
    @TypeConverters(PokeTypeConverter.class)
    private List<PokeType> types;

    @SerializedName("moves")
    @TypeConverters(PokeMoveConverter.class)
    private List<PokeMove> moves;

    private int storageLocation; //1 for internal, 2 for external

    private int security; //1 for encrypted, 2 for decrypted


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public List<PokeStat> getStats() {
        return stats;
    }

    public void setStats(List<PokeStat> stats) {
        this.stats = stats;
    }

    public List<PokeAbility> getAbilities() {
        return abilities;
    }

    public void setAbilities(List<PokeAbility> abilities) {
        this.abilities = abilities;
    }

    public List<PokeType> getTypes() {
        return types;
    }

    public void setTypes(List<PokeType> types) {
        this.types = types;
    }

    public List<PokeMove> getMoves() {
        return moves;
    }

    public void setMoves(List<PokeMove> moves) {
        this.moves = moves;
    }

    public int getStorageLocation() {
        return storageLocation;
    }

    public void setStorageLocation(int storageLocation) {
        this.storageLocation = storageLocation;
    }

    public int getSecurity() {
        return security;
    }

    public void setSecurity(int security) {
        this.security = security;
    }
}
