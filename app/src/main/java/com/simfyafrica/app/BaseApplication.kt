package com.simfyafrica.app

import android.support.multidex.MultiDexApplication
import com.crashlytics.android.answers.Answers
import io.fabric.sdk.android.Fabric



open class BaseApplication : MultiDexApplication(), IApplication {
    override fun onCreate() {
        super.onCreate()

        Fabric.with(this, Answers())
    }
}
