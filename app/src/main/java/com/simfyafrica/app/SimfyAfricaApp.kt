package com.simfyafrica.app

import android.support.v7.app.AppCompatDelegate
import com.simfyafrica.api.RetrofitService
import com.simfyafrica.inject.component.AppComponent
import com.simfyafrica.inject.component.DaggerAppComponent
import com.simfyafrica.inject.component.IAppComponent
import com.simfyafrica.inject.module.AppModule
import com.simfyafrica.pref.IPreferencesManager
import javax.inject.Inject

open class SimfyAfricaApp : BaseApplication(){

    var appComponent: IAppComponent? = null


    @Inject
    lateinit var preferencesManager: IPreferencesManager

    override fun onCreate() {
        super.onCreate()

        application = this
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
        (appComponent as AppComponent?)?.inject(this)


        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)


        //FIXME  -- Add fonts in the project or use roboto for simplification
        //Calligraphy
//        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
//                .setDefaultFontPath("fonts/PTSRegular.ttf")
//                .setFontAttrId(R.attr.fontPath)
//                .build())

    }

    companion object {

        lateinit var application: SimfyAfricaApp
    }
}